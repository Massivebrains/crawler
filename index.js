const fs = require('fs');
const { parse } = require('himalaya');
const http = require('http');
const fetch = require('node-fetch');

try {

    let app = http.createServer(function (req, res) {

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        const html = fs.readFileSync('./index.html', { encoding: 'utf8' });
        const json = parse(html);

        if (json.length < 1 || json[0].children == undefined)
            return res.end(JSON.stringify({ 'data': 'index.html file does not contain any children' }));

        let { children } = json[0];
        let data = [];

        for (i = 0; i <= children.length - 1; i++) {

            let child = children[i];
            if (child.type != 'element') continue;
            if (child.attributes[0].key != 'id') continue;
            if (child.attributes[0].value != 'mainDiv') continue;

            let name = price = location = image = carId = link = '';

            try {

                name = child.children[1].children[1].children[0].content;

            } catch (ex) {

                console.log(ex.message);
            }

            try {

                price = child.children[9].children[1].children[1].children[2].content;

            } catch (ex) {

                try {

                    price = child.children[11].children[1].children[1].children[2].content;

                } catch (ex) {

                    console.log(ex.message);
                }
            }

            try {

                location = child.children[7].children[13].children[0].content;

            } catch (ex) {

                console.log(ex.message);
            }

            try {

                image = child.children[5].children[1].attributes[4].value;

            } catch (ex) {

                console.log(ex.message);
            }

            try {

                link = child.children[5].children[1].attributes[6].value;
                carId = link.substring(link.lastIndexOf('?') + 1, link.lastIndexOf(')')).replace(/'/g, '');
                link = link.substring(link.lastIndexOf('(') + 1, link.lastIndexOf(')')).replace(/'/g, '');

            } catch (ex) {

                console.log(ex.message);
            }

            name = name.replace('\n', '').replace(/ {1,}/g, ' ').trim();
            price = price.replace('\n', '').replace(/ {1,}/g, ' ').trim();
            location = location.replace('\n', '').replace(/ {1,}/g, ' ').trim();
            image = image.replace('\n', '').replace(/ {1,}/g, ' ').trim();
            carId = carId.replace('\n', '').replace(/ {1,}/g, ' ').trim();

            data.push({

                name: name,
                price: price,
                location: location,
                image: image,
                link: link,
                carId: carId
            });
        }

        const headers = { Accept: 'application/json', 'Content-Type': 'application/json' };

        fs.writeFile('./index.html', '', function () {

            console.log('cleared file')
        });

        fetch('http://nova.ffb.com.ng/crawler', { method: 'POST', headers: headers, body: JSON.stringify({ cars: data }) })
            .then(resp => resp.json())
            .then(response => {

                res.end(JSON.stringify(response));

            }).catch(err => {

                res.end(JSON.stringify(err));

            });

    });

    app.listen(3000, 'localhost', () => {

        console.log('Magic happens on port 3000');
    });

} catch (ex) {

    console.log(ex.message);
}