const { parse } = require('himalaya');
const http = require('http');
url = require('url');
const fetch = require('node-fetch');

try {

    let app = http.createServer(function (req, res) {

        let query = url.parse(req.url, true).query;

        if (!query.url) {

            return res.end(JSON.stringify({ error: 'Pass ?url in the url' }));
        }
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        fetch(query.url).then(resp => resp.text()).then(html => {

            const json = parse(html);

            let vin = json[3].children[3].children[7].children[29].children[3].children[1].children[1].children[5].children[3].children[9].children[1].children[15].children[3].children[1].children[0].content;

            vin = vin.replace('\n', '').replace('"', '').replace(/ {1,}/g, ' ').trim();

            console.log(vin);

            res.end(JSON.stringify({ vin: vin }));

        }).catch(err => {

            res.end(JSON.stringify({ vin: '' }));

        });

    });

    app.listen(3001, 'localhost', () => {

        console.log('Magic happens on port 3001');
    });

} catch (ex) {

    console.log(ex.message);
}